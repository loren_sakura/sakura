﻿using UnityEngine;


namespace Sakura {
    public class CameraFollow : MonoBehaviour {

        #region Public Constants
        #endregion


        #region Protected Contants
        #endregion


        #region Public Properties
        public Controller2D target;
        public Vector2 focusAreaSize;
        public float verticalOffset;
        public float lookAheadDstX;
        public float lookSmoothTimeX;
        public float verticalSmoothTime;
        #endregion


        #region Protected Properties
        private FocusArea focusArea;
        private float currentLookAheadX;
        private float targetLookAheadX;
        private float lookAheadDirX;
        private float smoothLookVelocityX;
        private float smoothVelocityY;
        private bool lookAheadStopped;
        #endregion


        #region Public Methods
        #endregion


        #region Protected Methods
        #endregion


        #region Unity's Methods
        private void Awake() {
        }

        private void Start() {
            focusArea = new FocusArea(target.collider.bounds, focusAreaSize);
        }

        private void FixedUpdate() {
        }

        private void Update() {
        }

        private void LateUpdate() {
            focusArea.Update(target.collider.bounds);

            Vector2 focusPosition = focusArea.center + Vector2.up * verticalOffset;

            if (0.0f != focusArea.velocity.x) {
                lookAheadDirX = Mathf.Sign(focusArea.velocity.x);

                if ((Mathf.Sign(target.playerInput.x) == Mathf.Sign(focusArea.velocity.x)) && (0.0f != target.playerInput.x)) {
                    lookAheadStopped = false;
                    targetLookAheadX = lookAheadDirX * lookAheadDstX;
                }
                else {
                    if (!lookAheadStopped) {
                        lookAheadStopped = true;
                        targetLookAheadX = currentLookAheadX + (lookAheadDirX * lookAheadDstX - currentLookAheadX) / 4.0f;
                    }
                }
            }

            targetLookAheadX = lookAheadDirX * lookAheadDstX;
            currentLookAheadX = Mathf.SmoothDamp(currentLookAheadX, targetLookAheadX, ref smoothLookVelocityX, lookSmoothTimeX);

            focusPosition.y = Mathf.SmoothDamp(transform.position.y, focusPosition.y, ref smoothVelocityY, verticalSmoothTime);
            focusPosition += Vector2.right * currentLookAheadX;
            transform.position = (Vector3)focusPosition + Vector3.forward * -10.0f;
        }

        private void OnDrawGizmos() {
            Gizmos.color = new Color(1.0f, 0.0f, 0.0f, 0.5f);
            Gizmos.DrawCube(focusArea.center, focusAreaSize);
        }
        #endregion

    }
}