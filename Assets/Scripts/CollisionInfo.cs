﻿using UnityEngine;


namespace Sakura {
    public struct CollisionInfo {

        #region Public Constants
        #endregion


        #region Protected Contants
        #endregion


        #region Public Properties
        public bool below, above, left, right;

        public bool climbingSlope;
        public float slopeAngle, slopeAngleOld;

        public bool descendingSlope;
        public Vector3 velocityOld;

        public int faceDir;

        public bool fallingThroughPlatform;

        public bool slidingDownMaxSlope;
        public Vector2 slopeNormal;
        #endregion


        #region Protected Properties
        #endregion


        #region Public Methods
        public void Reset() {
            below = above = false;
            left = right = false;

            climbingSlope = false;
            slopeAngleOld = slopeAngle;
            slopeAngle = 0.0f;

            descendingSlope = false;

            slidingDownMaxSlope = false;
            slopeNormal = Vector2.zero;
        }
        #endregion


        #region Protected Methods
        #endregion

    }
}