﻿using UnityEngine;


namespace Sakura {
    public class Controller2D : RaycastController {

        #region Public Constants
        #endregion


        #region Protected Contants
        #endregion


        #region Public Properties
        public CollisionInfo collisions;
        public Vector2 playerInput;

        public float maxSlopeAngle = 80.0f;
        #endregion


        #region Protected Properties
        #endregion


        #region Public Methods
        public void Move(Vector2 moveAmount, Vector2 input, bool standingOnPlatform = false) {
            UpdateRaycastOrigins();
            collisions.Reset();
            collisions.velocityOld = moveAmount;
            playerInput = input;

            if (0.0f > moveAmount.y)
                DescendSlope(ref moveAmount);

            if (0.0f != moveAmount.x)
                collisions.faceDir = (int)Mathf.Sign(moveAmount.x);

            HorizontalCollisions(ref moveAmount);

            if (0.0f != moveAmount.y)
                VerticalCollisions(ref moveAmount);

            transform.Translate(moveAmount);

            if (standingOnPlatform)
                collisions.below = true;
        }

        public void Move(Vector2 moveAmount, bool standingOnPlatform) {
            Move(moveAmount, Vector2.zero, standingOnPlatform);
        }
        #endregion


        #region Protected Methods
        private void ResetFallingThroughPlatform() {
            collisions.fallingThroughPlatform = false;
        }

        private void HorizontalCollisions(ref Vector2 moveAmount) {
            float directionX = collisions.faceDir;
            float rayLength = Mathf.Abs(moveAmount.x) + skinWidth;

            if (Mathf.Abs(moveAmount.x) < skinWidth)
                rayLength = 2.0f * skinWidth;

            for (int i = 0; i < horizontalRayCount; i++) {
                Vector2 rayOrigin = ((-1.0f == directionX) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight);
                rayOrigin += Vector2.up * (horizontalRaySpacing * i);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

                Debug.DrawRay(rayOrigin, Vector2.right * directionX, Color.red);

                if (hit) {
                    if (0.0f == hit.distance)
                        continue;

                    float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);

                    if (0 == i && slopeAngle <= maxSlopeAngle) {
                        if (collisions.descendingSlope) {
                            collisions.descendingSlope = false;
                            moveAmount = collisions.velocityOld;
                        }
                        float distanceToSlopeStart = 0.0f;
                        if (slopeAngle != collisions.slopeAngleOld) {
                            distanceToSlopeStart = hit.distance - skinWidth;
                            moveAmount.x -= distanceToSlopeStart * directionX;
                        }
                        ClimbSlope(ref moveAmount, slopeAngle, hit.normal);
                        moveAmount.x += distanceToSlopeStart * directionX;
                    }

                    if (!collisions.climbingSlope || slopeAngle > maxSlopeAngle) {
                        moveAmount.x = (hit.distance - skinWidth) * directionX;
                        rayLength = hit.distance;

                        if (collisions.climbingSlope)
                            moveAmount.y = Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveAmount.x);

                        collisions.left = -1.0f == directionX;
                        collisions.right = 1.0f == directionX;
                    }
                }
            }
        }

        private void VerticalCollisions(ref Vector2 moveAmount) {
            float directionY = Mathf.Sign(moveAmount.y);
            float rayLength = Mathf.Abs(moveAmount.y) + skinWidth;

            for (int i = 0; i < verticalRayCount; i++) {
                Vector2 rayOrigin = ((-1.0f == directionY) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft);
                rayOrigin += Vector2.right * (verticalRaySpacing * i + moveAmount.x);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, collisionMask);

                Debug.DrawRay(rayOrigin, Vector2.up * directionY, Color.red);

                if (hit) {
                    if ("Through" == hit.collider.tag) {
                        if ((1.0f == directionY) || (0.0f == hit.distance))
                            continue;

                        if (collisions.fallingThroughPlatform)
                            continue;

                        if (-1.0f == playerInput.y) {
                            collisions.fallingThroughPlatform = true;
                            Invoke("ResetFallingThroughPlatform", 0.5f);
                            continue;
                        }
                    }

                    moveAmount.y = (hit.distance - skinWidth) * directionY;
                    rayLength = hit.distance;

                    if (collisions.climbingSlope)
                        moveAmount.x = moveAmount.y / Mathf.Tan(collisions.slopeAngle * Mathf.Deg2Rad) * Mathf.Sign(moveAmount.x);

                    collisions.below = -1.0f == directionY;
                    collisions.above = 1.0f == directionY;
                }
            }

            if (collisions.climbingSlope) {
                float directionX = Mathf.Sign(moveAmount.x);
                rayLength = Mathf.Abs(moveAmount.x) + skinWidth;
                Vector2 rayOrigin = ((-1.0f == directionX) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight) + Vector2.up * moveAmount.y;
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, collisionMask);

                if (hit) {
                    float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                    if (slopeAngle != collisions.slopeAngle) {
                        moveAmount.x = (hit.distance - skinWidth) * directionX;
                        collisions.slopeAngle = slopeAngle;
                        collisions.slopeNormal = hit.normal;
                    }
                }
            }
        }

        private void ClimbSlope(ref Vector2 moveAmount, float slopeAngle, Vector2 slopeNormal) {
            float moveDistance = Mathf.Abs(moveAmount.x);
            float climbVelocityY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;

            if (climbVelocityY >= moveAmount.y) {
                moveAmount.y = climbVelocityY;
                moveAmount.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(moveAmount.x);
                collisions.below = true;
                collisions.climbingSlope = true;
                collisions.slopeAngle = slopeAngle;
                collisions.slopeNormal = slopeNormal;
            }

        }

        private void DescendSlope(ref Vector2 moveAmount) {
            RaycastHit2D maxSlopeHitLeft = Physics2D.Raycast(raycastOrigins.bottomLeft, Vector2.down, Mathf.Abs(moveAmount.y) + skinWidth, collisionMask);
            RaycastHit2D maxSlopeHitRight = Physics2D.Raycast(raycastOrigins.bottomRight, Vector2.down, Mathf.Abs(moveAmount.y) + skinWidth, collisionMask);
            if (maxSlopeHitLeft ^ maxSlopeHitRight) {
                SlideDownMaxSlope(maxSlopeHitLeft, ref moveAmount);
                SlideDownMaxSlope(maxSlopeHitRight, ref moveAmount);
            }

            if (!collisions.slidingDownMaxSlope) {
                float directionX = Mathf.Sign(moveAmount.x);
                Vector2 rayOrigin = ((-1.0f == directionX) ? raycastOrigins.bottomRight : raycastOrigins.bottomLeft);
                RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.down, Mathf.Infinity, collisionMask);

                if (hit) {
                    float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                    if (0.0f != slopeAngle && slopeAngle <= maxSlopeAngle)
                        if (Mathf.Sign(hit.normal.x) == directionX)
                            if ((hit.distance - skinWidth) <= (Mathf.Tan(slopeAngle * Mathf.Deg2Rad) * Mathf.Abs(moveAmount.x))) {
                                float moveDistance = Mathf.Abs(moveAmount.x);
                                float descendVelocityY = Mathf.Sin(slopeAngle * Mathf.Deg2Rad) * moveDistance;
                                moveAmount.x = Mathf.Cos(slopeAngle * Mathf.Deg2Rad) * moveDistance * Mathf.Sign(moveAmount.x);
                                moveAmount.y -= descendVelocityY;

                                collisions.slopeAngle = slopeAngle;
                                collisions.descendingSlope = true;
                                collisions.below = true;
                                collisions.slopeNormal = hit.normal;
                            }
                }
            }
        }

        private void SlideDownMaxSlope(RaycastHit2D hit, ref Vector2 moveAmount) {
            if (hit) {
                float slopeAngle = Vector2.Angle(hit.normal, Vector2.up);
                if (slopeAngle > maxSlopeAngle) {
                    moveAmount.x = hit.normal.x * (Mathf.Abs(moveAmount.y) - hit.distance) / Mathf.Tan(slopeAngle * Mathf.Deg2Rad);

                    collisions.slopeAngle = slopeAngle;
                    collisions.slidingDownMaxSlope = true;
                    collisions.slopeNormal = hit.normal;
                }
            }
        }
        #endregion


        #region Unity's Methods
        protected override void Awake() {
            base.Awake();
        }

        protected override void Start() {
            base.Start();

            collisions.faceDir = 1;
        }

        private void FixedUpdate() {
        }

        private void Update() {
        }
        #endregion

    }
}