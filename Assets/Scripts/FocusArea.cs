﻿using UnityEngine;


namespace Sakura {
    public class FocusArea {

        #region Public Constants
        #endregion


        #region Protected Contants
        #endregion


        #region Public Properties
        public Vector2 center;
        public Vector2 velocity;
        #endregion


        #region Protected Properties
        private float left, right;
        private float top, bottom;
        #endregion


        public FocusArea(Bounds targetBounds, Vector2 size) {
            left = targetBounds.center.x - size.x / 2.0f;
            right = targetBounds.center.x + size.x / 2.0f;
            bottom = targetBounds.min.y - size.y / 2.0f;
            top = targetBounds.min.y + size.y / 2.0f;

            velocity = Vector2.zero;
            center = new Vector2((left + right) / 2.0f, (top + bottom) / 2.0f);
        }


        #region Public Methods
        public void Update(Bounds targetBounds) {
            float shiftX = 0.0f;
            if (targetBounds.min.x < left)
                shiftX = targetBounds.min.x - left;
            else if (targetBounds.max.x > right)
                shiftX = targetBounds.max.x - right;
            left += shiftX;
            right += shiftX;

            float shiftY = 0.0f;
            if (targetBounds.min.y < bottom)
                shiftY = targetBounds.min.y - bottom;
            else if (targetBounds.max.y > top)
                shiftY = targetBounds.max.y - top;
            top += shiftY;
            bottom += shiftY;

            center = new Vector2((left + right) / 2.0f, (top + bottom) / 2.0f);
            velocity = new Vector2(shiftX, shiftY);
        }

        #endregion


        #region Protected Methods
        #endregion

    }
}