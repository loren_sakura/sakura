﻿using UnityEngine;


namespace Sakura {
    public struct PassengerMovement {

        #region Public Constants
        #endregion


        #region Protected Contants
        #endregion


        #region Public Properties
        public Transform transform;
        public Vector3 velocity;
        public bool standingOnPlatform;
        public bool moveBeforePlatform;
        #endregion


        public PassengerMovement(Transform transform, Vector3 velocity, bool standingOnPlatform, bool moveBeforePlatform) {
            this.transform = transform;
            this.velocity = velocity;
            this.standingOnPlatform = standingOnPlatform;
            this.moveBeforePlatform = moveBeforePlatform;
        }


        #region Protected Properties
        #endregion


        #region Public Methods
        #endregion


        #region Protected Methods
        #endregion

    }
}