﻿using System.Collections.Generic;
using UnityEngine;


namespace Sakura {
    public class PlatformController : RaycastController {

        #region Public Constants
        #endregion


        #region Protected Contants
        #endregion


        #region Public Properties
        public LayerMask passengerMask;
        public Vector3[] localWaypoints;
        public float speed;
        public bool cyclic;
        public float waitTime;
        [Range(0.0f, 2.0f)]
        public float easeAmount;
        #endregion


        #region Protected Properties
        private List<PassengerMovement> passengerMovement;
        private Dictionary<Transform, Controller2D> passengerDictionary = new Dictionary<Transform, Controller2D>();
        private Vector3[] globalWaypoints;
        private int fromWaypointIndex;
        private float percentBetweenWaypoints;
        private float nextMoveTime;
        #endregion


        #region Public Methods
        #endregion


        #region Protected Methods
        private Vector3 CalculatePlatformMovement() {
            if (Time.time < nextMoveTime)
                return Vector3.zero;

            fromWaypointIndex %= globalWaypoints.Length;
            int toWaypointIndex = (fromWaypointIndex + 1) % globalWaypoints.Length;
            float distanceBetweenWaypoints = Vector3.Distance(globalWaypoints[fromWaypointIndex], globalWaypoints[toWaypointIndex]);
            percentBetweenWaypoints += Time.deltaTime * speed / distanceBetweenWaypoints;
            percentBetweenWaypoints = Mathf.Clamp01(percentBetweenWaypoints);
            float easedPercentBetweenWaypoints = Ease(percentBetweenWaypoints);

            Vector3 newPos = Vector3.Lerp(globalWaypoints[fromWaypointIndex], globalWaypoints[toWaypointIndex], easedPercentBetweenWaypoints);

            if (1.0f <= percentBetweenWaypoints) {
                percentBetweenWaypoints = 0.0f;
                fromWaypointIndex++;

                if (!cyclic)
                    if (globalWaypoints.Length - 1 <= fromWaypointIndex) {
                        fromWaypointIndex = 0;
                        System.Array.Reverse(globalWaypoints);
                    }
                nextMoveTime = Time.time + waitTime;
            }

            return newPos - transform.position;
        }

        private void CalculatePassengerMovement(Vector3 velocity) {
            HashSet<Transform> movedPassengers = new HashSet<Transform>();
            passengerMovement = new List<PassengerMovement>();
            float directionX = Mathf.Sign(velocity.x);
            float directionY = Mathf.Sign(velocity.y);

            // Vertically moving platform
            if (0.0f != velocity.y) {
                float rayLength = Mathf.Abs(velocity.y) + skinWidth;

                for (int i = 0; i < verticalRayCount; i++) {
                    Vector2 rayOrigin = ((-1.0f == directionY) ? raycastOrigins.bottomLeft : raycastOrigins.topLeft);
                    rayOrigin += Vector2.right * (verticalRaySpacing * i);
                    RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up * directionY, rayLength, passengerMask);

                    if (hit && (0.0f != hit.distance))
                        if (!movedPassengers.Contains(hit.transform)) {
                            movedPassengers.Add(hit.transform);
                            float pushX = ((1.0f == directionY) ? velocity.x : 0.0f);
                            float pushY = velocity.y - (hit.distance - skinWidth) * directionY;

                            passengerMovement.Add(new PassengerMovement(hit.transform, new Vector3(pushX, pushY), (1.0f == directionY), true));
                        }
                }
            }

            // Horizontally moving platform
            if (0.0f != velocity.x) {
                float rayLength = Mathf.Abs(velocity.x) + skinWidth;

                for (int i = 0; i < horizontalRayCount; i++) {
                    Vector2 rayOrigin = ((-1.0f == directionX) ? raycastOrigins.bottomLeft : raycastOrigins.bottomRight);
                    rayOrigin += Vector2.up * (horizontalRaySpacing * i);
                    RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, rayLength, passengerMask);

                    if (hit && (0.0f != hit.distance))
                        if (!movedPassengers.Contains(hit.transform)) {
                            movedPassengers.Add(hit.transform);
                            float pushX = velocity.x - (hit.distance - skinWidth) * directionX;
                            float pushY = -skinWidth;

                            passengerMovement.Add(new PassengerMovement(hit.transform, new Vector3(pushX, pushY), false, true));
                        }
                }
            }

            // Passenger on top of a horizontally or downward moving platform
            if ((-1.0f == directionY) || ((0.0f == velocity.y) && (0.0f != velocity.x))) {
                float rayLength = skinWidth * 2.0f;

                for (int i = 0; i < verticalRayCount; i++) {
                    Vector2 rayOrigin = raycastOrigins.topLeft + Vector2.right * (verticalRaySpacing * i);
                    RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.up, rayLength, passengerMask);

                    if (hit && (0.0f != hit.distance))
                        if (!movedPassengers.Contains(hit.transform)) {
                            movedPassengers.Add(hit.transform);
                            float pushX = velocity.x;
                            float pushY = velocity.y;

                            passengerMovement.Add(new PassengerMovement(hit.transform, new Vector3(pushX, pushY), true, false));
                        }
                }
            }
        }

        private void MovePassengers(bool beforeMovePlatform) {
            foreach (PassengerMovement passenger in passengerMovement) {
                if (!passengerDictionary.ContainsKey(passenger.transform))
                    passengerDictionary.Add(passenger.transform, passenger.transform.GetComponent<Controller2D>());

                if (passenger.moveBeforePlatform == beforeMovePlatform)
                    passengerDictionary[passenger.transform].Move(passenger.velocity, passenger.standingOnPlatform);
            }
        }

        private float Ease(float x) {
            float a = easeAmount + 1;
            return Mathf.Pow(x, a) / (Mathf.Pow(x, a) + Mathf.Pow(1 - x, a));
        }
        #endregion


        #region Unity's Methods
        protected override void Awake() {
            base.Awake();
        }

        protected override void Start() {
            base.Start();

            globalWaypoints = new Vector3[localWaypoints.Length];
            for (int i = 0; i < localWaypoints.Length; i++)
                globalWaypoints[i] = localWaypoints[i] + transform.position;
        }

        private void FixedUpdate() {
        }

        private void Update() {
            UpdateRaycastOrigins();

            Vector3 velocity = CalculatePlatformMovement();

            CalculatePassengerMovement(velocity);

            MovePassengers(true);
            transform.Translate(velocity);
            MovePassengers(false);
        }

        private void OnDrawGizmos() {
            if (null != localWaypoints) {
                Gizmos.color = Color.red;
                float size = 0.3f;

                for (int i = 0; i < localWaypoints.Length; i++) {
                    Vector3 globalWaypointPos = (Application.isPlaying ? globalWaypoints[i] : localWaypoints[i] + transform.position);
                    Gizmos.DrawLine(globalWaypointPos - Vector3.up * size, globalWaypointPos + Vector3.up * size);
                    Gizmos.DrawLine(globalWaypointPos - Vector3.left * size, globalWaypointPos + Vector3.left * size);
                }
            }
        }
        #endregion

    }
}