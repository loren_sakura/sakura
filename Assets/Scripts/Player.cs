﻿using UnityEngine;


namespace Sakura {
    [RequireComponent(typeof(Controller2D))]
    public class Player : MonoBehaviour {

        #region Public Constants
        #endregion


        #region Protected Contants
        #endregion


        #region Public Properties
        public float maxJumpHeight = 4.0f;
        public float timeToJumpApex = 0.4f;

        public float wallSlideSpeedMax = 3.0f;
        public Vector2 wallJumpClimb;
        public Vector2 wallJumpOff;
        public Vector2 wallLeap;
        public float wallStickTime = 0.25f;

        public float minJumpHeight = 1.0f;
        #endregion


        #region Protected Properties
        private Controller2D controller;

        private float gravity;
        private Vector3 velocity;

        private float moveSpeed = 6.0f;

        private float maxJumpVelocity;

        private float velocityXSmoothing;
        private float accelerationTimeAirborne = 0.2f;
        private float accelerationTimeGrounded = 0.1f;

        private float timeToWallUnstick;

        private float minJumpVelocity;

        private Vector2 directionalInput;

        private bool wallSliding;
        private int wallDirX;
        #endregion


        #region Public Methods
        public void SetDirectionalInput(Vector2 input) {
            directionalInput = input;
        }

        private int i = 100;
        public void OnJumpInputDown() {
            if (wallSliding) {
                if (wallDirX == directionalInput.x) {
                    velocity.x = -wallDirX * wallJumpClimb.x;
                    velocity.y = wallJumpClimb.y;
                }
                else if (0.0f == directionalInput.x) {
                    velocity.x = -wallDirX * wallJumpOff.x;
                    velocity.y = wallJumpOff.y;
                }
                else {
                    velocity.x = -wallDirX * wallLeap.x;
                    velocity.y = wallLeap.y;
                }
            }

            if (controller.collisions.below) {
                if (controller.collisions.slidingDownMaxSlope) {
                    if (-Mathf.Sign(controller.collisions.slopeNormal.x) != directionalInput.x) {
                        // Not jumping againts max slope
                        velocity.y = maxJumpVelocity * controller.collisions.slopeNormal.y;
                        velocity.x = maxJumpVelocity * controller.collisions.slopeNormal.x;
                    }
                }
                else 
                    velocity.y = maxJumpVelocity;

                i = 0;
            }
        }

        public void OnJumpInputUp() {
            if (velocity.y > minJumpVelocity)
                velocity.y = minJumpVelocity;
        }

        #endregion


        #region Protected Methods
        private void CalculateVelocity() {
            float targetVelocityX = directionalInput.x * moveSpeed;
            velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below ? accelerationTimeGrounded : accelerationTimeAirborne));
            velocity.y += gravity * Time.fixedDeltaTime;
        }

        private void HandleWallSliding() {
            wallDirX = (controller.collisions.left ? -1 : 1);
            wallSliding = false;
            if ((controller.collisions.left || controller.collisions.right) && !controller.collisions.below && 0.0f > velocity.y) {
                wallSliding = true;

                if (velocity.y < -wallSlideSpeedMax)
                    velocity.y = -wallSlideSpeedMax;

                if (0.0f < timeToWallUnstick) {
                    velocityXSmoothing = 0.0f;
                    velocity.x = 0.0f;

                    if ((directionalInput.x != wallDirX) && (0.0f != directionalInput.x))
                        timeToWallUnstick -= Time.fixedDeltaTime;
                    else
                        timeToWallUnstick = wallStickTime;
                }
                else
                    timeToWallUnstick = wallStickTime;
            }
        }
        #endregion


        #region Unity's Methods
        private void Awake() {
        }

        private void Start() {
            controller = GetComponent<Controller2D>();

            gravity = -(2.0f * maxJumpHeight) / Mathf.Pow(timeToJumpApex, 2.0f);
            maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
            minJumpVelocity = Mathf.Sqrt(2.0f * Mathf.Abs(gravity) * minJumpHeight);
        }

        private void FixedUpdate() {
            float prevVelocityY = velocity.y;
            CalculateVelocity();
            HandleWallSliding();

            // using prev velocity method
            Vector2 moveAmount = new Vector2( velocity.x, (velocity.y + prevVelocityY) / 2.0f) * Time.fixedDeltaTime;

            //// using gravity method
            //Vector2 moveAmount = velocity * Time.fixedDeltaTime;
            //moveAmount.y -= gravity * Mathf.Pow(Time.fixedDeltaTime, 2.0f) / 2.0f;

            controller.Move(moveAmount, directionalInput);
            if (controller.collisions.below || controller.collisions.above) {
                if (controller.collisions.slidingDownMaxSlope)
                    velocity.y += controller.collisions.slopeNormal.y * -gravity * Time.fixedDeltaTime;
                else
                    velocity.y = 0.0f;
            }

            if (i++ < 55)
                print(string.Format("n: {0}, u: {1}, v: {2}, s: {3}, h:{4}", i, prevVelocityY, velocity.y, moveAmount.y, transform.position.y));
        }

        private void Update() {
        }
        #endregion

    }
}