﻿using UnityEngine;


namespace Sakura {
    [RequireComponent(typeof(Player))]
    public class PlayerInput : MonoBehaviour {

        #region Public Constants
        #endregion


        #region Protected Contants
        #endregion


        #region Public Properties
        #endregion


        #region Protected Properties
        private Player player;
        #endregion


        #region Public Methods
        #endregion


        #region Protected Methods
        #endregion


        #region Unity's Methods
        private void Awake() {
        }

        private void Start() {
            player = GetComponent<Player>();
        }

        private void FixedUpdate() {
        }

        private void Update() {
            Vector2 directionalInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            player.SetDirectionalInput(directionalInput);

            if (Input.GetButtonDown("Jump")) {
                player.OnJumpInputDown();
            }

            if (Input.GetButtonUp("Jump")) {
                player.OnJumpInputUp();
            }
        }
        #endregion

    }
}