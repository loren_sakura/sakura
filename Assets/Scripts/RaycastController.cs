﻿using UnityEngine;


namespace Sakura {
    [RequireComponent(typeof(BoxCollider2D))]
    public class RaycastController : MonoBehaviour {

        #region Public Constants
        #endregion


        #region Protected Contants
        protected const float skinWidth = 0.015f;
        private const float dstBetweenRays = 0.25f;
        #endregion


        #region Public Properties

        public LayerMask collisionMask;

        public BoxCollider2D collider;
        #endregion


        #region Protected Properties
        protected RaycastOrigins raycastOrigins;

        protected int horizontalRayCount;
        protected int verticalRayCount;
        protected float horizontalRaySpacing;
        protected float verticalRaySpacing;
        #endregion


        #region Public Methods
        #endregion


        #region Protected Methods
        protected void UpdateRaycastOrigins() {
            Bounds bounds = collider.bounds;
            bounds.Expand(skinWidth * -2.0f);

            raycastOrigins.bottomLeft = new Vector2(bounds.min.x, bounds.min.y);
            raycastOrigins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
            raycastOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
            raycastOrigins.topRight = new Vector2(bounds.max.x, bounds.max.y);
        }

        private void CalculateRaySpacing() {
            Bounds bounds = collider.bounds;
            bounds.Expand(skinWidth * -2.0f);

            float boundsWidth = bounds.size.x;
            float boundsHeight = bounds.size.y;

            horizontalRayCount = Mathf.RoundToInt( boundsHeight / dstBetweenRays);
            verticalRayCount = Mathf.RoundToInt(boundsWidth / dstBetweenRays);

            horizontalRaySpacing = bounds.size.y / (horizontalRayCount - 1);
            verticalRaySpacing = bounds.size.x / (verticalRayCount - 1);
        }
        #endregion


        #region Unity's Methods
        protected virtual void Awake() {
            collider = GetComponent<BoxCollider2D>();
        }

        protected virtual void Start() {
            CalculateRaySpacing();
        }

        private void FixedUpdate() {
        }

        private void Update() {
        }
        #endregion

    }
}