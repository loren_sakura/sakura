﻿using UnityEngine;


namespace Sakura {
    public struct RaycastOrigins {

        #region Public Constants
        #endregion


        #region Protected Contants
        #endregion


        #region Public Properties
        public Vector2 bottomLeft, bottomRight;
        public Vector2 topLeft, topRight;
        #endregion


        #region Protected Properties
        #endregion


        #region Public Methods
        #endregion


        #region Protected Methods
        #endregion

    }
}